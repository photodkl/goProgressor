package main

import (
	"time"

	"gitee.com/shirdonl/goProgressor"
)

func main() {
	goprogressor.Start()            // start rendering
	bar := goprogressor.AddBar(100) // Add a new bar

	// optionally, append and prepend completion and elapsed time
	bar.AppendCompleted()
	bar.PrependElapsed()

	for bar.Incr() {
		time.Sleep(time.Millisecond * 20)
	}
}
